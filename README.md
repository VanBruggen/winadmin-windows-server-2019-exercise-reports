# Windows Server 2019 exercise reports

Language: Polish.

PDF reports of my exercises in Windows Server 2019, as part of the Chosen IT Systems Administration subject on my CS studies. Server Manager, MMC console, DHCP and Firewall.